import pandas as pd
from sklearn.preprocessing import LabelEncoder


class DataLoader(object):
    def fit(self, dataset):
        self.dataset = dataset.copy()

    def load_data(self):
        # binning with pd.cut
        self.dataset['Age_categorical'] = pd.cut(self.dataset['Age'], 6)

        # drop columns
        cols_to_drop = [
            'Age',
            'Region_Code',
            'Annual_Premium',
            'Policy_Sales_Channel',
            'Vintage',
        ]
        self.dataset = self.dataset.drop(cols_to_drop, axis=1)

        # encode labels
        cols_to_label = [
            "Gender",
            "Age_categorical",
            "Driving_License",
            "Previously_Insured",
            "Vehicle_Age",
            "Vehicle_Damage"
        ]

        le = LabelEncoder()
        for column in cols_to_label:
            le.fit(self.dataset[column])
            self.dataset[column] = le.transform(self.dataset[column])

        # columns_combination
        self.dataset["Danger"] = (self.dataset["Gender"]
                                  + self.dataset["Driving_License"]
                                  + self.dataset["Vehicle_Damage"])

        # normalize
        cols_to_normalize = [
            "Age_categorical",
            "Vehicle_Age",
            "Danger"
        ]

        for column in cols_to_normalize:
            incol = self.dataset[column]
            self.dataset[column] = (incol - incol.min()) / (incol.max() - incol.min())

        return self.dataset
