# Description
A Flask webapp, ready for Dockerization. Provides a simple API for querying a pretrained Classifier.


## Dataset from
https://www.kaggle.com/datasets/anmolkumar/health-insurance-cross-sell-prediction


## Used
* Flask
* Docker
* scikit-learn
* pandas


## Ignore files
`specifications.json`, `dataset.py`, `trainer.py` are parts of automatic evaluation by a third party. Can be ignored.


# Example use

`val_x` is a pandas dataframe, yet unclassified

`192.168.1.103` can be changed to IP seen upon starting the flaskapp

```
loader = DataLoader()
loader.fit(val_x)
X = loader.load_data()

req_data = {'data': json.dumps(X.to_dict())}
response = requests.get('http://192.168.1.103:8000/predict', data=req_data)
api_predict = response.json()['prediction']
```